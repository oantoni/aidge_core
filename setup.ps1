# Helper setup tool to automatically build aidge_core on Windows.

# Requirements
################################################################################
# You have either VS BuildTools or VS Community already present on your 
# system, with the build tools installed.
# If not, download Visual Studio Community here:
# https://visualstudio.microsoft.com/fr/vs/community/
# Make sure to install the "Desktop Development with C++" workload.
# Run this script in a Powershell console with Administrator rights in order to
# automatically install the dependencies, or just execute the second part if you
# already have all the dependencies satisfied.

# Enable or disable automatic installation of requirements
# Run .\setup.ps1 -install_reqs:$false to disable it
param ([bool]$install_reqs=$true)

# Default install path is .\install_cpp
if (-not $env:AIDGE_INSTALL_PATH)
{
    $env:AIDGE_INSTALL_PATH = $(Join-Path $pwd install_cpp)
}

# 1. Setup environment
################################################################################
if ($install_reqs)
{
    # Install Chocolatey
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    # Install dependencies
    choco install cmake.install --installargs '"ADD_CMAKE_TO_PATH=System"' -Y
    choco install git -Y
    choco install python -Y
    # Update PATH
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

# 2. Compile & install aidge_core
################################################################################
mkdir -Force build_cpp
mkdir -Force $env:AIDGE_INSTALL_PATH
Set-Location build_cpp
cmake -DCMAKE_INSTALL_PREFIX:PATH=$env:AIDGE_INSTALL_PATH -DCMAKE_BUILD_TYPE=Debug -DDOSANITIZE=OFF ..
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
cmake --build . -j2
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
cmake --install . --config Debug
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
# Optional: run the unit tests
ctest --output-on-failure
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
Set-Location $PSScriptRoot
