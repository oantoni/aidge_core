/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Gather.hpp"

#include <cstddef>  // std::size_t
#include <cstdint>  // std::int64_t
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"

void Aidge::Gather_OpImpl::forward() {
    const Gather_Op& op = dynamic_cast<const Gather_Op&>(mOp);
    const auto axis = op.template getAttr<std::int64_t>("Axis");

    const std::size_t axisIdx = axis>=0 ?
                                axis :
                                static_cast<std::size_t>(axis) + op.getInput(0)->dims().size();

    std::size_t postAxisElems = 1;
    for (std::size_t i = axisIdx + 1; i < op.getInput(0)->dims().size(); ++i) {
        postAxisElems *= op.getInput(0)->dims()[i];
    }
    std::size_t preAxisElems = 1;
    for (std::size_t i = 0; i < axisIdx; ++i) {
        preAxisElems *= op.getInput(0)->dims()[i];
    }

    const auto indices = op.template getAttr<std::vector<std::int64_t>>("Indices");
    std::size_t outputOffset = 0;
    for (std::size_t i=0; i<preAxisElems; ++i)
    {
        for(std::size_t j=0; j<indices.size(); ++j)
        {
            const std::size_t idx = indices[j] >= 0 ? indices[j] : static_cast<std::size_t>(indices[j]) + op.getInput(0)->dims()[axisIdx];
            op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(i * postAxisElems * op.getInput(0)->dims()[axisIdx] + idx * postAxisElems), postAxisElems, outputOffset);
            outputOffset += postAxisElems;
        }
    }
}

const std::string Aidge::Gather_Op::Type = "Gather";

bool Aidge::Gather_Op::forwardDims(bool /*allowDataDependency*/) {
    // check inputs have been associated
    if (!getInput(0)) {
        AIDGE_THROW_OR_ABORT(std::runtime_error, "Input was not connected");
    }

    if (!getInput(0)->empty()) {
        std::vector<DimSize_t> outDims = getInput(0)->dims();
        const std::vector<DimSize_t> gatheredShape = this->template getAttr<GatherAttr::GatheredShape>();
        // TODO: check indices and gatheredShape

        const std::int64_t axisIdx = this->template getAttr<GatherAttr::Axis>() >= 0 ?
                                        this->template getAttr<GatherAttr::Axis>() :
                                        this->template getAttr<GatherAttr::Axis>() + outDims.size();
        outDims.erase(outDims.begin() + static_cast<std::size_t>(axisIdx));
        if (!gatheredShape.empty())
        {
            outDims.insert(outDims.cbegin() + static_cast<std::size_t>(axisIdx),
                            gatheredShape.cbegin(),
                            gatheredShape.cend());
        }

        mOutputs[0]->resize(outDims);
        return true;
    }

    return false;
}

void Aidge::Gather_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Gather_Op>::exists({name})) {
        SET_IMPL_MACRO(Gather_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Gather_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}
