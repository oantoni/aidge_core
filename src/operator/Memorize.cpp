/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Memorize.hpp"

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::Memorize_OpImpl::getNbRequiredData(
    Aidge::IOIndex_t inputIdx) const
{
    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);
    const unsigned int scheduleStep = op.template getAttr<MemorizeAttr::ScheduleStep>();

    if (scheduleStep == 0 && inputIdx == 0) {
        // No data input is required for the initial step.
        // Initialization data is required however.
        return Elts_t::NoneElts();
    }
    else if (scheduleStep > 0 && inputIdx == 1) {
        // No initialization data is required after the initial step.
        return Elts_t::NoneElts();
    }
    else {
        return OperatorImpl::getNbRequiredData(inputIdx);
    }
}

Aidge::Elts_t Aidge::Memorize_OpImpl::getRequiredMemory(const Aidge::IOIndex_t outputIdx,
                                                         const std::vector<Aidge::DimSize_t> &/*inputsSize*/) const {
    assert(mOp.getRawOutput(outputIdx) && "requires valid output");

    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);
    const unsigned int scheduleStep = op.template getAttr<MemorizeAttr::ScheduleStep>();
    const unsigned int endStep = op.template getAttr<MemorizeAttr::EndStep>();

    if (endStep > 0 && outputIdx == 1 && scheduleStep >= endStep) {
        return Elts_t::NoneElts();
    }
    else {
        return Elts_t::DataElts(op.getOutput(outputIdx)->size());
    }
}

void Aidge::Memorize_OpImpl::updateConsummerProducer() {
    OperatorImpl::updateConsummerProducer();

    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);
    const unsigned int scheduleStep = op.template getAttr<MemorizeAttr::ScheduleStep>();
    const unsigned int endStep = op.template getAttr<MemorizeAttr::EndStep>();
    AIDGE_ASSERT(endStep == 0 || scheduleStep <= endStep, "cannot update consumer producer anymore, number of cycles exceeded");
}

void Aidge::Memorize_OpImpl::forward() {
    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);
    const unsigned int forwardStep = op.template getAttr<MemorizeAttr::ForwardStep>();
    const unsigned int endStep = op.template getAttr<MemorizeAttr::EndStep>();
    AIDGE_ASSERT(endStep == 0 || forwardStep <= endStep, "cannot forward anymore, number of cycles exceeded");

    if (forwardStep == 0) {
        op.getOutput(0)->getImpl()->copy(op.getInput(1)->getImpl()->rawPtr(), op.getInput(1)->size());
    }
    else {
        op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(), op.getInput(0)->size());
    }
}

const std::string Aidge::Memorize_Op::Type = "Memorize";

void Aidge::Memorize_Op::updateConsummerProducer() {
    Operator::updateConsummerProducer();
    ++this->template getAttr<MemorizeAttr::ScheduleStep>();
    this->template getAttr<MemorizeAttr::ForwardStep>() = 0;
}

bool Aidge::Memorize_Op::forwardDims(bool /*allowDataDependency*/) {
    for (size_t i = 0; i < 2; ++i) {
        if (!getInput(i)) {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "{}: input #{} should be associated with a Tensor", type(), i);
        }
    }

    // Only require one of the input to have dims defined
    // Otherwise, forwardDims() won't converge!
    if (!(getInput(0)->empty())) {
        const auto expectedDims =  getInput(0)->dims();
        mOutputs[0]->resize(expectedDims);
        return true;
    }
    else if (!(getInput(1)->empty())) {
        const auto expectedDims =  getInput(1)->dims();
        mOutputs[0]->resize(expectedDims);
        return true;
    }

    return false;
}

bool Aidge::Memorize_Op::dimsForwarded() const {
    // Only check the output dims
    bool forwarded = true;
    // check outputs have been filled
    for (IOIndex_t i = 0; i < nbOutputs(); ++i) {
        forwarded &= !(getOutput(i)->empty());
    }
    return forwarded;
}

void Aidge::Memorize_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Memorize_Op>::exists({name})){
        SET_IMPL_MACRO(Memorize_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Memorize_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

void Aidge::Memorize_Op::forward() {
    Operator::forward();
    ++this->template getAttr<MemorizeAttr::ForwardStep>();
    this->template getAttr<MemorizeAttr::ScheduleStep>() = 0;
}
