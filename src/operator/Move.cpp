/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Move.hpp"

void Aidge::Move_OpImpl::forward() {
    const Move_Op& op = dynamic_cast<const Move_Op&>(mOp);
    op.getOutput(0)->copyFrom(*(op.getInput(0)));
}

const std::string Aidge::Move_Op::Type = "Move";

void Aidge::Move_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Move_Op>::exists({mInputs[0]->getImpl()->backend(), name})) {
        SET_IMPL_MACRO(Move_Op, *this, {mInputs[0]->getImpl()->backend(), name});
    }
    else {
        mImpl = std::make_shared<Move_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}
