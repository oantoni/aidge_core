import os
from jinja2 import Environment, FileSystemLoader


def generate_file(file_path: str, template_path: str, **kwargs) -> None:
    """Generate a file at `file_path` using the jinja template located at `file_path`.

    kwargs are used to fill the template.

    :param file_path: path where to generate the file
    :type file_path: str
    :param template_path: Path to the template to use for code generation
    :type template_path: str
    """
    # Get directory name of the file
    dirname = os.path.dirname(file_path)

    # If directory doesn't exist, create it
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    # Get directory name and name of the template
    template_dir = os.path.dirname(template_path)
    template_name = os.path.basename(template_path)

    # Select template
    template = Environment(loader=FileSystemLoader(
        template_dir)).get_template(template_name)

    # Generate file
    content = template.render(kwargs)
    with open(file_path, mode="w", encoding="utf-8") as message:
        message.write(content)

def generate_str(template_path:str, **kwargs) -> str:
    """Generate a string using the jinja template located at `file_path`.
    kwargs are used to fill the template.

    :param template_path: Path to the template to use for code generation
    :type template_path: str
    :return: A string of the interpreted template
    :rtype: str
    """
    dirname = os.path.dirname(template_path)
    filename = os.path.basename(template_path)
    template = Environment(loader=FileSystemLoader(dirname)).get_template(filename)
    return template.render(kwargs)
