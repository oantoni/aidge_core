/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>
#include <cstddef>
#include <cstdint>  //std::uint16_t
#include <random>
#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/TensorUtils.hpp"
#include "aidge/backend/cpu/data/TensorImpl.hpp"

using namespace Aidge;

TEST_CASE("[backend/cpu/data] Tensor", "[TensorImpl]") {
    Tensor x = Array3D<int, 2, 2, 2>{{{{1, 2}, {3, 4}}, {{5, 6}, {7, 8}}}};

    SECTION("Access to array") {
        x = Array3D<int, 2, 2, 2>{{{{1, 2}, {3, 4}}, {{5, 6}, {7, 8}}}};
        REQUIRE(static_cast<int *>(x.getImpl()->rawPtr())[0] == 1);
        REQUIRE(static_cast<int *>(x.getImpl()->rawPtr())[7] == 8);
    }
}

TEST_CASE("Tensor fill", "[TensorImpl][fill]") {
  SECTION("Instantiate batches independantly") {
    // initialization with 0s
    std::shared_ptr<Tensor> concatenatedTensor= std::make_shared<Tensor>(Array2D<int, 3, 5>{});
    //concatenatedTensor->print();

    std::shared_ptr<Tensor> myTensor1 = std::make_shared<Tensor>(Array1D<int, 5>{{1,2,3,4,5}});
    std::shared_ptr<Tensor> myTensor2 = std::make_shared<Tensor>(Array1D<int, 5>{{6,7,8,9,10}});
    std::shared_ptr<Tensor> myTensor3 = std::make_shared<Tensor>(Array1D<int, 5>{{11,12,13,14,15}});

    // use copy function from implementation
    concatenatedTensor->getImpl()->copy(myTensor1->getImpl()->rawPtr(), 5, 0);
    concatenatedTensor->getImpl()->copy(myTensor2->getImpl()->rawPtr(), 5, 5);
    concatenatedTensor->getImpl()->copy(myTensor3->getImpl()->rawPtr(), 5, 10);
    // concatenatedTensor->print();

    std::shared_ptr<Tensor> expectedTensor= std::make_shared<Tensor>(Array2D<int, 3, 5>{
      {{1,2,3,4,5},
      {6,7,8,9,10},
      {11,12,13,14,15}}
    });
    // expectedTensor->print();

    REQUIRE(*concatenatedTensor == *expectedTensor);
  }
}
