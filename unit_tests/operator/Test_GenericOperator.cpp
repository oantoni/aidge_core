/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/operator/GenericOperator.hpp"
#include "aidge/graph/GraphView.hpp"
#include <cstddef>

using namespace Aidge;

TEST_CASE("[core/operators] GenericOp(add & get attributes)", "[Operator]") {
    SECTION("INT") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        const char* key = "intAttr";
        Testop.addAttr(key, int(5));
        int registeredVal = Testop.getAttr<int>(key);
        REQUIRE(registeredVal == 5);
    }
    SECTION("LONG") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        long value = 3;
        const char* key = "longAttr";
        Testop.addAttr(key, value);
        REQUIRE(Testop.getAttr<long>(key) == value);
    }
    SECTION("FLOAT") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        float value = 2.0;
        const char* key = "floatAttr";
        Testop.addAttr(key, value);
        REQUIRE(Testop.getAttr<float>(key) == value);
    }
     SECTION("VECTOR<BOOL>") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        std::vector<bool> value = {true, false, false, true, true};
        const char* key = "vect";
        Testop.addAttr(key, value);

        REQUIRE(Testop.getAttr<std::vector<bool>>(key).size() == value.size());
        for (std::size_t i=0; i < value.size(); ++i){
            REQUIRE(Testop.getAttr<std::vector<bool>>(key)[i] == value[i]);
        }
    }
    SECTION("VECTOR<INT>") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        std::vector<int> value = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        const char* key = "vect";
        Testop.addAttr(key, value);

        REQUIRE(Testop.getAttr<std::vector<int>>(key).size() == value.size());
        for (std::size_t i=0; i < value.size(); ++i){
            REQUIRE(Testop.getAttr<std::vector<int>>(key)[i] == value[i]);
        }
    }
    SECTION("MULTIPLE PARAMS") {
        /*
        Goal : Test that the offsets are well done by adding different attributes with different size.
        */
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        Testop.addAttr<long>("longAttr", 3);
        Testop.addAttr<float>("floatAttr", 2.0);
        Testop.addAttr<uint8_t>("uint8Attr", 5);
        Testop.addAttr<long long>("llAttr", 10);
        REQUIRE(Testop.getAttr<long>("longAttr") == 3);
        REQUIRE(Testop.getAttr<float>("floatAttr") == 2.0);
        REQUIRE(Testop.getAttr<uint8_t>("uint8Attr") == 5);
        REQUIRE(Testop.getAttr<long long>("llAttr") == 10);
    }
}

TEST_CASE("[core/operator] GenericOp(type check)", "[Operator]") {
    SECTION("WRONG TYPE FOR GETTER") {
        GenericOperator_Op Testop("TestOp", 1, 1, 1);
        Testop.addAttr<long>("longAttr", 3);

        // This line should raise a failled assert
        REQUIRE_THROWS(Testop.getAttr<int>("longAttribute"));
    }
}
