/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cstddef>  // std::size_t
#include <memory>
#include <random>   // std::random_device, std::mt19937, std::uniform_int_distribution
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Pow.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {
TEST_CASE("[core/operator] Pow_Op(forwardDims)", "[Pow][forwardDims]") {
    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(1, 5);

    // Create Pow Operator
    std::shared_ptr<Node> myPow = Pow();
    auto op = std::static_pointer_cast<OperatorTensor>(myPow -> getOperator());

    // input_0
    std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
    op -> associateInput(0,T0);
    // input_1
    std::shared_ptr<Tensor> T1 = std::make_shared<Tensor>();
    op -> associateInput(1,T1);

    /**
     * @todo Special case: scalar not handled yet by
     * ``OperatorTensor::forwardDims()``
     */
    // SECTION("Scalar / Scalar") {
    //     // input_0
    //     T0->resize({});

    //     // input_1
    //     T1->resize({});

    //     REQUIRE_NOTHROW(op->forwardDims());
    //     REQUIRE((op->getOutput(0)->dims() == std::vector<std::size_t>()));
    // }
    // SECTION("Scalar / +1-D") {
    //     // a scalar is compatible with any other Tensor
    //     // input_0
    //     T0->resize({});

    //     for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {

    //         // input_1
    //         const std::size_t nb_dims = nbDimsDist(gen);
    //         std::vector<std::size_t> dims(nb_dims);
    //         for (std::size_t i = 0; i < nb_dims; ++i) {
    //             dims[i] = dimsDist(gen);
    //         }
    //         T1->resize(dims);

    //         REQUIRE_NOTHROW(op->forwardDims());
    //         REQUIRE((op->getOutput(0)->dims()) == dims);
    //     }
    // }
    // SECTION("+1-D / Scalar") {
    //     // a scalar is compatible with any other Tensor
    //     // input_1
    //     T1->resize({});

    //     for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {

    //         // input_0
    //         const std::size_t nb_dims = nbDimsDist(gen);
    //         std::vector<std::size_t> dims(nb_dims);
    //         for (std::size_t i = 0; i < nb_dims; ++i) {
    //             dims[i] = dimsDist(gen);
    //         }
    //         T0->resize(dims);

    //         REQUIRE_NOTHROW(op->forwardDims());
    //         REQUIRE((op->getOutput(0)->dims()) == dims);
    //     }
    // }
    SECTION("+1-D / +1-D") {
        // same size
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            const std::size_t nb_dims = nbDimsDist(gen) + 1;
            std::vector<std::size_t> dims0(nb_dims);
            for (std::size_t i = 0; i < nb_dims; ++i) {
                dims0[i] = dimsDist(gen) + 1;
            }

            T0->resize(dims0);
            T1->resize(dims0);
            REQUIRE_NOTHROW(op->forwardDims());
            REQUIRE((op->getOutput(0)->dims()) == dims0);
        }

        // broadcast
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            const std::size_t nb_dims = nbDimsDist(gen) + 1;
            std::vector<std::size_t> dims0(nb_dims);
            for (std::size_t i = 0; i < nb_dims; ++i) {
                dims0[i] = dimsDist(gen) + 2;
            }
            std::vector<std::size_t> dimsOut = dims0;
            std::vector<std::size_t> dims1 = dims0;
            for (std::size_t i = 0; i < nb_dims; ++i) {
                if (dimsDist(gen) <= 5) {
                    dims1[i] = 1;
                }
            }
            dims1.erase(dims1.cbegin(), dims1.cbegin() + std::min(nbDimsDist(gen), nb_dims-1));

            T0->resize(dims0);
            T1->resize(dims1);

            REQUIRE_NOTHROW(op->forwardDims());
            REQUIRE((op->getOutput(0)->dims()) == dimsOut);

            // input_0 - wrong
            // T1->resize({dims[0] + 1});
            std::vector<std::size_t> dims1_wrong = dims1;
            for (std::size_t i = 0; i < dims1.size(); ++i) {
                ++dims1_wrong[i];
            }
            T1->resize(dims1_wrong);
            REQUIRE(dims0 != dims1_wrong);
            REQUIRE_THROWS(op->forwardDims());
        }
    }
}
} // namespace Aidge
