
#include <catch2/catch_test_macros.hpp>

#include "aidge/graphRegex/GraphFsmInterpreter.hpp"


using namespace Aidge;
TEST_CASE("GraphFsmInterpreter", "GraphFsmInterpreter") {

    SECTION("Construction") {
        std::vector<std::shared_ptr<ConditionalInterpreter>> allTest = {
            std::make_shared<ConditionalInterpreter>("A","true==true"),
            std::make_shared<ConditionalInterpreter>("B","true==true"),
            std::make_shared<ConditionalInterpreter>("C","true==true")
        };

        //GraphFsmInterpreter("A->B",allTest);
        std::shared_ptr<GraphFsmInterpreter>  fsmGenerator = std::make_shared<GraphFsmInterpreter>("A->B",allTest);
        std::shared_ptr<FsmGraph> fsm = fsmGenerator->interpret();

        
        REQUIRE(fsm->getNodes().size() == 3);
        REQUIRE(fsm->getStartNodes().size() == 1);
        REQUIRE(fsm->getEdge().size() == 2);

        for(auto node : fsm->getNodes()){
            if(node->isValid()){
                REQUIRE(node->getEdges().size() == 0);
            }else{
                REQUIRE(node->getEdges().size() == 1);
            }
           
        }

        
    }





}