/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <set>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/graphRegex/GraphRegex.hpp"
#include "aidge/recipes/Recipes.hpp"

namespace Aidge {


TEST_CASE("Examples", "[GraphMatching]") {
    auto g1 = Sequential({
        Producer({16, 3, 512, 512}, "dataProvider"),
        Conv(3, 4, {5, 5}, "conv1"),
        ReLU(),
        PaddedConv(4, 8, {5, 5}, "conv2", {1, 1}, {2, 2, 2, 2}),
        ReLU(),
        PaddedConv(8, 16, {5, 5}, "conv3", {1, 1}, {2, 2, 2, 2}),
        ReLU()
    });

    expandMetaOps(g1);
    g1->save("Test_examples");

    auto regex = std::make_shared<GraphRegex>();
    regex->setKeyFromGraph(g1);
    regex->addQuery("Pad->Conv->ReLU");
    // Won't work, wrong number of matches:
    //regex->addQuery("Pad*->Conv->ReLU*");

    const auto match = regex->match(g1);
    REQUIRE(match.size() == 2);

    for (const auto& solution : match) {
        REQUIRE(solution->getAll().size() == 3);
    }
}

}  // namespace Aidge