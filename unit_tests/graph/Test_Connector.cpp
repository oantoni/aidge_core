/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/graph/Connector.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/graph/Testing.hpp"

using namespace Aidge;

TEST_CASE("[core/graph] Connector(Constructor)") {
    SECTION("Empty") {
        Connector x = Connector();
        REQUIRE(x.index() == gk_IODefaultIndex);
        REQUIRE(x.node() == nullptr);
    }
    SECTION("0 output") {
        std::shared_ptr<Node> node = GenericOperator("Producer", 1, 0, 0);
        Connector x = Connector(node);
        REQUIRE(x.index() == gk_IODefaultIndex);
        REQUIRE(x.node() == node);
    }
    SECTION("1 output") {
        std::shared_ptr<Node> node = GenericOperator("ReLU", 1, 0, 1);
        Connector x = Connector(node);
        REQUIRE(x.index() == 0);
        REQUIRE(x.node() == node);
    }
    SECTION("Several outputs") {
        std::shared_ptr<Node> node = GenericOperator("Split", 1, 0, 2);
        Connector x = Connector(node);
        REQUIRE(x.index() == gk_IODefaultIndex);
        REQUIRE(x.node() == node);
    }
}

TEST_CASE("Connector connections Node", "[Connector]") {
    SECTION("0 input / 0 output") {
        std::shared_ptr<Node> fic = GenericOperator("Display", 0, 0, 0);
        Connector x;
        x = (*fic)({});
        REQUIRE(x.node() == fic);
    }
    SECTION("1 input / 0 output") {
        std::shared_ptr<Node> fic = GenericOperator("Loss", 1, 0, 0);
        Connector x;
        x = (*fic)({x});
        REQUIRE(x.node() == fic);
    }
    SECTION("0 input / 1 output") { // Producers
        std::shared_ptr<Node> fic = GenericOperator("Producer", 0, 0, 1);
        Connector x = (*fic)({});
        REQUIRE(x.node() == fic);
    }
    SECTION("1 input / 1 output") {
        std::shared_ptr<Node> fic = GenericOperator("Conv", 1, 0, 1);
        Connector x(GenericOperator("Producer",0,0,1));
        x = (*fic)({x});
        REQUIRE(x.node() ==fic);
    }
    SECTION("2+ inputs / 1 output") { // ElemWise
        std::shared_ptr<Node> fic = GenericOperator("fictive", 3, 0, 1);
        Connector x1(GenericOperator("fictive",0,0,1));
        Connector x2(GenericOperator("fictive",0,0,1));
        Connector x3(GenericOperator("fictive",0,0,1));
        Connector x = (*fic)({x1, x2, x3});
        REQUIRE(x.node() ==fic);
    }
    SECTION("1 input / 2+ outputs") { // Slice
        std::shared_ptr<Node> fic = GenericOperator("fictive", 1, 0, 3);

        Connector x(GenericOperator("fictive2", 0, 0, 1));
        Connector y;
        REQUIRE_NOTHROW(y = (*fic)({x}));
        REQUIRE(y[0].node() == fic);
        REQUIRE(y[1].node() == fic);
        REQUIRE(y[2].node() == fic);
    }
}

TEST_CASE("GraphGeneration from Connector", "[GraphView]") {

    auto node01 = GenericOperator("Conv", 0, 0, 1,"g_conv1");
    auto node02 = GenericOperator("ReLU", 1, 0, 1,"g_relu");
    auto node03 = GenericOperator("g_maxpool1", 1, 0, 1);
    auto node04 = GenericOperator("g_conv2_par1", 1, 0, 1);
    auto node05 = GenericOperator("g_relu2_par1", 1, 0, 1);
    auto node06 = GenericOperator("g_conv2_par2", 1, 0, 1);
    auto node07 = GenericOperator("g_relu2_par2", 1, 0, 1);
    auto node08 = GenericOperator("g_concat", 2, 0, 1);
    auto node09 = GenericOperator("g_conv3", 1, 0, 1);
    auto node10 = GenericOperator("g_matmul1", 2, 0, 1);
    Connector a = (*node01)({});
    Connector x = (*node02)({a});
    x = (*node03)({x});
    Connector y = (*node04)({x});
    y = (*node05)({y});
    Connector z = (*node06)({x});
    z = (*node07)({z});
    x = (*node08)({y, z});
    x= (*node09)({x});
    x = (*node10)({a, x});
    std::shared_ptr<GraphView> gv = generateGraph({x});
    // gv->save("GraphGeneration");
    REQUIRE(nodePtrTo(gv->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({}));
    REQUIRE(nodePtrTo(gv->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"g_matmul1", 0}}));
}

TEST_CASE("Connector connection GraphView", "[Connector]") {
    SECTION("1 input") {
        Connector x = Connector();
        auto prod = GenericOperator("Producer", 0, 0, 1);
        auto g = Residual({
            GenericOperator("g_conv1", 1, 0, 1),
            GenericOperator("g_relu", 1, 0, 1),
            GenericOperator("g_maxpool1", 1, 0, 1),
            Parallel({
                Sequential({GenericOperator("g_conv2_par1", 1, 0, 1), GenericOperator("g_relu2_par1", 1, 0, 1)}),
                Sequential({GenericOperator("g_conv2_par2", 1, 0, 1), GenericOperator("g_relu2_par2", 1, 0, 1)})
            }),
            GenericOperator("g_concat", 2, 0, 1),
            GenericOperator("g_conv3", 1, 0, 1),
            GenericOperator("g_matmul1", 2, 0, 1)
        });
        REQUIRE(nodePtrTo(g->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"g_conv1", 0}}));
        REQUIRE(nodePtrTo(g->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"g_matmul1", 0}}));

        x = (*prod)({});
        x = (*g)({x});
        std::shared_ptr<GraphView> g2 = generateGraph({x});
        std::shared_ptr<GraphView> g3 = g;
        g3->add(prod);
        REQUIRE(*g3 == *g2);
    }
    SECTION("2+ inputs") {
        Connector x = (*GenericOperator("Producer", 0, 0, 1))({});
        Connector y = (*GenericOperator("Producer", 0, 0, 1))({});
        Connector z = (*GenericOperator("Producer", 0, 0, 1))({});
        auto g = Sequential({GenericOperator("ElemWise", 3, 0, 1),
            Parallel({
                Sequential({GenericOperator("g_conv2_par1", 1, 0, 1), GenericOperator("g_relu2_par1", 1, 0, 1)}),
                Sequential({GenericOperator("g_conv2_par2", 1, 0, 1), GenericOperator("g_relu2_par2", 1, 0, 1)}),
                Sequential({GenericOperator("g_conv2_par3", 1, 0, 1), GenericOperator("g_relu2_par3", 1, 0, 1)})
            }),
            GenericOperator("g_concat", 3, 0, 1),
            GenericOperator("g_conv3", 1, 0, 1)
        });
        REQUIRE(nodePtrTo(g->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"ElemWise", 0}, {"ElemWise", 1}, {"ElemWise", 2}}));
        REQUIRE(nodePtrTo(g->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"g_conv3", 0}}));

        x = (*g)({x, y, z});
        std::shared_ptr<GraphView> gv = generateGraph({x});
        REQUIRE(nodePtrTo(gv->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({}));
        REQUIRE(nodePtrTo(gv->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"g_conv3", 0}}));
        // gv->save("MultiInputSequentialConnector");
        REQUIRE(gv->inputNodes().size() == 0U);
    }
}

TEST_CASE("Connector Mini-graph", "[Connector]") {
    Connector x = Connector();
    Connector y = Connector();
    x = (*GenericOperator("Producer", 0, 0, 1))({});
    y = (*GenericOperator("Producer", 0, 0, 1))({});
    for (int i = 0; i<5; ++i) {
        x = (*GenericOperator("Conv", 1, 0, 1))({x});
    }
    y = (*GenericOperator("ElemWise", 2, 0, 1))({y, x});
    std::shared_ptr<GraphView> g = generateGraph({y});
    REQUIRE(nodePtrTo(g->getOrderedInputs()) == std::vector<std::pair<std::string, IOIndex_t>>({}));
    REQUIRE(nodePtrTo(g->getOrderedOutputs()) == std::vector<std::pair<std::string, IOIndex_t>>({{"ElemWise", 0}}));
    // g->save("TestGraph");
}

TEST_CASE("Structural descrition - Sequential", "[GraphView]") {
    // SECTION("Empty Sequence") {
    //     std::shared_ptr<GraphView> g1 = Sequential(); // Not supported
    //     REQUIRE(g1->getNodes() == std::set<std::shared_ptr<Node>>());
    //     REQUIRE(g1->inputNodes() == std::set<std::shared_ptr<Node>>());
    //     REQUIRE(g1->outputNodes() == std::set<std::shared_ptr<Node>>());
    // }
    SECTION("1-element Sequence") {
        std::shared_ptr<Node> fic = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Sequential({fic});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic}));
    }
    SECTION("several-elements simple Sequence") {
        std::shared_ptr<Node> fic1 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic2 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic3 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Sequential({fic1, fic2, fic3});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic1}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic3}));
    }

}

TEST_CASE("Structural description - Parallel", "[GraphView]") {
    // SECTION("Empty Parallel") {
    //     std::shared_ptr<GraphView> g1 = Parallel(); // Not supported
    //     REQUIRE(g1->getNodes() == std::set<std::shared_ptr<Node>>());
    //     REQUIRE(g1->inputNodes() == std::set<std::shared_ptr<Node>>());
    //     REQUIRE(g1->outputNodes() == std::set<std::shared_ptr<Node>>());
    // }
    SECTION("1-element Parallel") {
        std::shared_ptr<Node> fic = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Parallel({fic});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic}));
    }
    SECTION("several-elements simple Parallel") {
        std::shared_ptr<Node> fic1 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic2 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic3 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Parallel({fic1, fic2, fic3});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3}));
    }
    SECTION("1 Graph in Parallel") {
        std::shared_ptr<Node> fic1 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic2 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic3 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Parallel({Sequential({fic1, fic2, fic3})});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic1}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic3}));
    }
    SECTION("several Sequential in Parallel") {
        std::shared_ptr<Node> fic1 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic2 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic3 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic4 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic5 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<Node> fic6 = GenericOperator("node1", 1, 0, 1);
        std::shared_ptr<GraphView> g2 = Parallel({Sequential({fic1, fic2, fic3}),Sequential({fic4, fic5, fic6})});
        REQUIRE(g2->getNodes() == std::set<std::shared_ptr<Node>>({fic1, fic2, fic3, fic4, fic5, fic6}));
        REQUIRE(g2->inputNodes() == std::set<std::shared_ptr<Node>>({fic1, fic4}));
        REQUIRE(g2->outputNodes() == std::set<std::shared_ptr<Node>>({fic3, fic6}));
    }
}

TEST_CASE("Strucutral Description - Complex Graph", "[GraphView]") {
    std::shared_ptr<Node> firstLayer = GenericOperator("first", 1, 0, 1);
    auto g = Sequential({firstLayer,
                    GenericOperator("l2", 1, 0, 1),
                    Parallel({Sequential({GenericOperator("conv1",1, 0, 1), GenericOperator("relu1", 1, 0, 1)}),
                            Sequential({GenericOperator("conv2", 1, 0, 1), GenericOperator("relu2", 1, 0, 1)})}),
                    GenericOperator("concat", 2, 0, 1),
                    GenericOperator("lastLayer", 1, 0, 1)});
    REQUIRE(g->getNodes().size() == 8U);
    REQUIRE(g->inputNodes() == std::set<std::shared_ptr<Node>>({firstLayer}));
}
