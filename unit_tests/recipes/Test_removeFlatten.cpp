/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <set>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

TEST_CASE("[cpu/recipies] RemoveFlatten", "[RemoveFlatten][recipies]") {
  std::shared_ptr<Node> flatten =
      GenericOperator("Flatten", 1, 0, 1, "myFlatten");
  std::shared_ptr<Node> fc0 = FC(10, 10, "FC_1");
  std::shared_ptr<Node> fc1 = FC(10, 10, "FC_2");
  std::shared_ptr<Node> prod = Producer(std::array<DimSize_t, 10>(), "myProd");

  SECTION("flatten last layer : nothing removed because pattern searched is "
          "Flatten=>FC") {
    std::shared_ptr<Aidge::GraphView> g = Sequential({fc0, flatten});

    removeFlatten(g);

    CHECK(g->getOrderedOutputs().size() == 1);
    CHECK(g->getOrderedOutputs()[0].first == flatten);

    CHECK(g->getOrderedInputs().size() == 1);
    CHECK(g->getOrderedInputs()[0].first == fc0);
    
    CHECK(fc0->getParent(0) == nullptr);
    CHECK(fc0->getChildren(0).size() == 1);
    CHECK(g->rootNode() == fc0);
  }
  SECTION("flatten first layer : flatten removed") {
    auto g = Sequential({flatten, fc0});

    removeFlatten(g);

    CHECK(g->getOrderedInputs().size() == 1);
    CHECK(g->getOrderedInputs()[0].first == fc0);
    
    CHECK(g->getOrderedOutputs().size() == 1);
    CHECK(g->getOrderedOutputs()[0].first == fc0);
    
    CHECK(fc0->getParent(0) == nullptr);
    CHECK(fc0->getChildren(0).size() == 0);
    CHECK(g->rootNode() == fc0);
  }
  SECTION("flatten middle layer") {

    auto g = Sequential({fc0, flatten, fc1});

    removeFlatten(g);

    CHECK(g->getOrderedInputs().size() == 1);
    CHECK(g->getOrderedInputs()[0].first == fc0);

    CHECK(g->getOrderedOutputs().size() == 1);
    CHECK(g->getOrderedOutputs()[0].first == fc1);
    
    CHECK(fc1->getParent(0) == fc0);
    CHECK(fc0->getChildren(0)[0] == fc1);
    CHECK(g->rootNode() == fc0);
  }
  SECTION("flatten right after a producer") {
    auto g = Sequential({prod, flatten, fc0});
    // prod->addChild(flatten, 0);
    // flatten->addChild(fc0, 0);
    // auto g = std::make_shared<GraphView>({prod, flatten, fc0});

    removeFlatten(g);

    CHECK(g->getOrderedInputs().size() == 0);
    
    CHECK(g->getOrderedOutputs().size() == 1);
    CHECK(g->getOrderedOutputs()[0].first == fc0);
    
    CHECK(fc0->getParent(0) == prod);
    CHECK(fc0->getChildren(0).size() == 0);

    CHECK(g->rootNode() == prod);
  }
}

} // namespace Aidge
