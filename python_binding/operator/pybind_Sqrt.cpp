/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Sqrt.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Sqrt(py::module& m) {
    py::class_<Sqrt_Op, std::shared_ptr<Sqrt_Op>, OperatorTensor>(m, "SqrtOp", py::multiple_inheritance())
    .def("get_inputs_name", &Sqrt_Op::getInputsName)
    .def("get_outputs_name", &Sqrt_Op::getOutputsName);
    declare_registrable<Sqrt_Op>(m, "SqrtOp");
    m.def("Sqrt", &Sqrt, py::arg("name") = "");
}
}  // namespace Aidge
